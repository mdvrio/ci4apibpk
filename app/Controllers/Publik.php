<?php

namespace App\Controllers;

use App\Models\PublikModel;
use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;

// class Publik extends BaseController
class Publik extends ResourceController
{
    use ResponseTrait;
    protected $publikModel;
    public function __construct()
    {
        $this->publikModel = new PublikModel();
        $this->db      = \Config\Database::connect();
    }

    public function index()
    {
        $this->db      = \Config\Database::connect();
        $builder = $this->db->table('publik');
        $query   = $builder->get()->getResult();  // Produces: SELECT * FROM mytable
        $data['publik'] = $query;
        return view('publik/publik', $data);
    }

    // public function show($id = null)
    // {                
    //     $data = [            
    //         'publik' => $this->publikModel->getPublik($id)
    //     ];

    //     //jika komik tidak ada di tabel
    //     if (empty($data['publik'])) {
    //         throw new \CodeIgniter\Exceptions\PageNotFoundException('Data untuk' . $id . ' tidak ditemukan.');
    //     }
    //     return view('publik/publik/$1', $data);
    // }

    public function new()
    {
        // session();
        $data = [            
            'validation' => \Config\Services::validation()
        ];
        return view('publik/formcreatepublik', $data);
    }

    public function create()
    {
        //validasi input
        if (!$this->validate([
            'nama' => [
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} harus diisi'
                ]
                ],
                'jeniskelamin' => [
                    'rules' => 'required',
                    'errors' => [
                        'required' => 'jenis kelamin harus diisi'
                    ]
                ]
        ])) {
            // $validation = \Config\Services::validation();
            // return redirect()->to('publik')->withInput()->with('validation', $validation);
            // return redirect()->to('publik')->withInput();
            session()->setFlashdata('error', $this->validator->listErrors());
            return redirect()->to('/publik/new')->withInput();
        } else {
            print_r($this->request->getVar());
        }

        // ambil gambar
        // $fileSampul = $this->request->getFile('sampul');

        // apakah file yang diupload ada atau tidak
        // if ($fileSampul->getError() == 4) {
        //     $namaSampul = 'default.png';
        // } else {
        //     // pindahkan file ke folder img
        //     $fileSampul->move('img');
        //     //ambil nama file sampul
        //     $namaSampul = $fileSampul->getName();
        // }


        //dapetin slugnya (paling)
        // $slug = url_title($this->request->getVar('judul'), '-', true);

        $this->publikModel->save([
            'nama' => $this->request->getVar('nama'),
            // 'slug' => $slug,
            'jeniskelamin' => $this->request->getVar('jeniskelamin'),
            'asaldaerah' => $this->request->getVar('asaldaerah'),
            'status' => $this->request->getVar('status')
        ]);

        session()->setFlashdata('pesan', 'Data berhasl ditambahkan!');

        return redirect()->to('publik');
    }

    public function edit($id = null)
    {
        $data = [
            'title' => 'Form Ubah Data Komik',
            'validation' => \Config\Services::validation(),
            'komik' => $this->komikModel->getKomik($id)
        ];
        return view('publik/publik/$1/edit', $data);
    }

    public function update($id = null)
    {
        // cek judul
        $komikLama = $this->komikModel->getKomik($this->request->getVar('slug'));
        if ($komikLama['judul'] == $this->request->getVar('judul')) {
            $rule_judul = 'required';
        } else {
            $rule_judul = 'required|is_unique[komik.judul]';
        }
        //validasi input
        if (!$this->validate([
            'judul' => [
                'rules' => $rule_judul,
                'errors' => [
                    'required' => '{field} komik harus diisi',
                    'is_unique' => '{field} komik sudah ada'
                ]
            ],
            'sampul' => [
                'rules' => 'max_size[sampul,1024]|is_image[sampul]|mime_in[sampul,image/jpg,image/jpeg,image/png]',
                'errors' => [
                    'max_size' => 'ukuran gambar max 1MB',
                    'is_image' => 'yang anda pilih bukan gambar',
                    'mime_in' => 'yang anda pilih bukan gambar'
                ]
            ]
        ])) {
            // $validation = \Config\Services::validation();
            return redirect()->to('/komik/edit/' . $this->request->getVar('slug'))->withInput();
            // ->with('validation', $validation);
        }
            $fileSampul = $this->request->getFile('sampul');

            //cek gambar berubah atau engga
            if($fileSampul->getError() == 4) {
                $namaSampul = $this->request->getVar('sampulLama');                
            } else {
                // generate nama file random
                $namaSampul = $fileSampul->getRandomName();
                // pindahkan gambar
                $fileSampul->move('img', $namaSampul);
                // hapus file lama
                unlink('img/' . $this->request->getVar('sampulLama'));
            }

        $slug = url_title($this->request->getVar('judul'), '-', true);
        $this->komikModel->save([
            'id' => $id,
            'judul' => $this->request->getVar('judul'),
            'slug' => $slug,
            'penulis' => $this->request->getVar('penulis'),
            'penerbit' => $this->request->getVar('penerbit'),
            'sampul' => $namaSampul
        ]);

        session()->setFlashdata('pesan', 'Data berhasl diubah');

        return redirect()->to('/publik');
    }

    public function delete($id = null)
    {
        $this->komikModel->delete($id);
        session()->setFlashdata('pesan', 'Data berhasl dihapus!');
        return redirect()->to('publik/publik/$1');
    }
}
